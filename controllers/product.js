const Product = require("../model/Product");
const auth = require("../auth");

// Create product (Admin only)
module.exports.createProduct = async (reqBody, userData) => {
	if(userData.isAdmin){
		let newProduct = new Product ({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price,
			stocks : reqBody.stocks
		})
		return newProduct.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			}
		})
	} else {
		return `Please try again.`
	}
	
}


// Retrieving all products
module.exports.getAllProducts = () => {
	return Product.find({isActive : true}).then(result => {
		if(result == null || result == undefined) {
	    return "Product/s don't exist."
	  }
	  else {
      return result;
    }
	})
}

// Retrieving single product
module.exports.getProduct = (reqParams) => {
	return Product.findOne({_id : reqParams.productId}).then(result => {
		return result;
	}).catch((error) => {
		return `Product don't exist`
	})
}


// Update a product (Admin Only)
module.exports.updateProduct = (user, reqParams, reqBody) => {
	if (user.isAdmin) {
		let updateProduct = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}
		return Product.findByIdAndUpdate(reqParams.productId, updateProduct)
		.then((course, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})
	}
	else {
		return "You don't the have access. Please try Again!";
	}
}

// Archiving a product (Admin Only)
module.exports.archiveProduct = async (userData, reqParams) => {
	if(userData.isAdmin){
		return Product.findByIdAndUpdate({_id : reqParams.productId}, {isActive: false}).then(result => {
			return result.save().then((updatedResult, error) => {
				if(error){
					return `You may try again!`
				} else {
					return `Archiving Success! ${updatedResult}`;
				}
			});
		})
	}
	else{
		return `You don't have any access to make changes.`
	}
}

/*

Create Product Sample Workflow
 1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
 2. API validates JWT, returns false if validation fails.
 3. If validation successful, API creates product using the contents of the request body.
 
Retrieve All Active Products Sample Workflow
 1. A GET request is sent to the /products endpoint.
 2. API retrieves all active products and returns them in its response.

Retrieve Single Product Sample Workflow
 1. A GET request is sent to the /products/:productId endpoint.
 2. API retrieves product that matches productId URL parameter and returns it in its response.

Update Product Sample Workflow
 1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId endpoint.
 2. API validates JWT, returns false if validation fails.
 3. If validation successful, API finds product with ID matching the productId URL parameter and overwrites its info with those from the request body.

Archive Product Sample Workflow
 1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/archive endpoint.
 2. API validates JWT, returns false if validation fails.
 3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to false.
 */
 