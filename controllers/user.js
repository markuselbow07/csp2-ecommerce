const User = require("../model/User");
const auth = require("../auth");

/*
const User = require("../model/User");
const Product = require("../model/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth")

*/
// password encryption
const bcrypt = require("bcrypt");

// Retrieve all users (Admin Only)
module.exports.getAllUsers = async (userData) => {
	if(userData.isAdmin){
		return User.find().then(result => {
			if(result === null || result === undefined) {
	      return `User/s do not exist.`
	    }
	    else{
      	for(let i = 0; i < result.length; i++){
      		result[i].password = "****";
      	}
        return result;
      }
		})
	}
	else{
		return `Access Failed!`
	}
}

//

// Retrieving All User (Non-Admin)
module.exports.getAllListUser = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return `Failed! You may try again.`;
		} else {
			return `Sign-up Success!`;
		}
	})
}

// User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return `Email doesn't exist. Kindly try again`;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
				}
			else{
				return `Wrong password!`;
			}
		}	
	})
}

// Setting Admin Access First
module.exports.setFirstAdmin = (userId) => {
	return User.findById(userId).then((result, error) => {
		if(error){
			return false;
		}
		result.isAdmin = true;

		return result.save().then((updateUser, err) => {
			if(error){
				return false;
			}
			else{
				return `${result.email} is now the admin.`;
			}
		})
	})
}

// Set admin access
module.exports.setAsAdmin = async (userData, userId, reqBody) => {
	if(userData.isAdmin) {
		return await User.findById(userId).then((result,error) =>{
			if(error){
				return "User don't exist."
			}

			else{
				result.isAdmin = reqBody.isAdmin;
				return result.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						return `Access for ${result.email} has been updated.`;
					}
				})
			}
		})
	}
	else{
		return "Access Failed!";
	}
}
