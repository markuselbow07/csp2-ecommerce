const Order = require("../model/Order");

// Create Order (User)
module.exports.createOrder = async (user, reqBody) => {
	if (user.isUser) {
		let newOrder = new Order({
			totalAmount : reqBody.totalAmount,
			purchasedOn : reqBody.purchasedOn,
			otherDetails : reqBody.otherDetails
		})

		return newOrder.save().then((order, error) => {
			if (error) {
				return false;
			}
			else {
				return ('Order creation success!');
			}
		})
	}
	else {
		return "You don't have access";
	}
}

// Retrieve all orders
module.exports.retrieveAllOrders = () => {
	return Order.find().then(result => {
		return result;
	})
}

// Retrieve User's Orders
module.exports.retrieveOrder = (reqParams) => {
	return Order.findById(reqParams.productId).then(result => {
		return result;
	})
}
