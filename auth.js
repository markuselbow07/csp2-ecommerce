const jwt = require("jsonwebtoken");
const secret = "ECommerceAPI"

// Token creation
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	// Generates a JSON web token using the jwt's sign method
	return jwt.sign(data, secret, {});
}

// Token Verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		console.log(token);
	
		token = token.slice(7, token.length);

		// Validate the token
		return jwt.verify(token, secret, (err, data) => {

			// JWT invalid
			if(err){
				return res.send({auth: "Failed! JWT inserted is invalid."});
			}
			else{
				// Proceed with next function
				next();
			}
		})
	}
	// token - don't exist
	else{
		return res.send({auth: "Failed!The token does not exist."});
	}
}

// Token decryption
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null;
			}
			else{
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
	else{
		return null;
	}
}
