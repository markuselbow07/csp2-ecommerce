const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
/*
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");

*/
const orderRoutes = require("./routes/order");
const userRoutes = require("./routes/user");
// const orderRoutes = require("./routes/order");
const productRoutes = require("./routes/product");

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Connect MongoDB Atlas

mongoose.connect("mongodb+srv://markusElbow:Magyarorszag7@cluster0.oy0gf.mongodb.net/capstone-2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true

	}
);


//to check if we are connected to MongoDB
mongoose.connection.once('open',() => console.log('Now connected to MongoDB Atlas'));

app.use("/users", userRoutes);
app.use("/products", productRoutes);


//environment variable port or port 4000
app.listen(process.env.PORT || 4000, () => {
	console.log(`API on Port ${ process.env.PORT || 4000 } is now online`)
});

/* Token Bearer
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxODlkMDE2NThkNmEyODZkNGJkNjA5NSIsImVtYWlsIjoia3lvaGVpdGFrYW5vQG1haWwuY29tIiwiaXNBZG1pbiI6dHJ1ZSwiaWF0IjoxNjM2NDI0MDU5fQ.Xy5q9Txs3xU2hY1sZIiwElp7WlfPLJ5FBTfHDQ5V8uk

*/