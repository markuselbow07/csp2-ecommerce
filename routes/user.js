const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");
/*
const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

*/
// Route for Registering a User
router.post("/register", (req, res) => {
	userController.registerUser(req.body)
	.then(resultFromController => res.send(`${resultFromController}`));
});

// Retrieving All Product (Non-Admin)
router.get("/list", (req, res) => {
	userController.getAllListUser().then(resultFromController => res.send(resultFromController));
})

// Route for Retrieving All Users (Admin Only)
router.get('/all', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getAllUsers(userData).then(result => res.send(result));
})

// Route for Authenticating a User
router.post("/login", (req, res) => {
	userController.loginUser(req.body)
	.then(resultFromController => res.send(resultFromController));
});


// Route for Setting the User First Admin
router.put("/setFirstAdmin/:id", (req, res) => {
	userController.setFirstAdmin(req.params.id)
	.then(resultFromController => res.send(resultFromController));
})


//Route for Setting the User's Admin Access (Admin Only)
router.put("/:id/setAsAdmin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.setAsAdmin(userData, req.params.id, req.body)
	.then(resultFromController => res.send(resultFromController));
});

module.exports = router;
