const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Create Product (Admin Only)
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.createProduct(req.body, userData)
	.then(result => res.send(result));
});

// Retrieve All Products
router.get('/', (req, res) => {
	productController.getAllProducts()
	.then(result => res.send(result));
});

// Retrieve a Specific Product
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params)
	.then(result => res.send(result));
});

// Update a Product (Admin Only)
router.put("/:productId", auth.verify, (req, res) => {

	const user = auth.decode(req.headers.authorization);

	productController.updateProduct(user, req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
})

// Archive Specific Product (Admin Only)
router.put('/:productId/archive', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	productController.archiveProduct(userData, req.params)
	.then(result => res.send(result));
});


module.exports = router;
