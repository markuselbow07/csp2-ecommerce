const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const userController = require("../controllers/user");
const auth = require("../auth");

// Create order
router.post("/users/checkout", /*auth.verify,*/ (req, res) => {
	
	/*const user = auth.decode(req.headers.authorization);*/

	orderController.createOrder(user, req.body)
	.then(resultFromController => res.send(resultFromController));
})

// retrieve all orders
router.get("/users/orders", (req, res) => {
	productController.retrieveAllProducts()
	.then(resultFromController => res.send(resultFromController));
})

// retrieve authenticated user's orders
router.get("/users/orders", (req, res) => {
	productController.retrieveAllProducts()
	.then(resultFromController => res.send(resultFromController));
})


/* Session's Deliverables
 - Create Order (NON-admin only)
 - Retrieve All Orders (admin only)
 - Retrieve Authenticated User's Orders (NON-admin only)
 - Initial deployment of API to Heroku

Create Order Sample Workflow
 - An authenticated NON-admin user sends a POST request containing a JWT in its header to the /users/checkout endpoint.
 - API validates user identity via JWT, returns false if validation fails.
 - If validation successful, API creates order using the contents of the request body.

Retrieve All Orders Sample Workflow
 - A GET request containing a JWT in its header is sent to the /users/orders endpoint.
 - API validates user is an admin via JWT, returns false if validation fails.
 - If validation is successful, API retrieves all orders and returns them in its response.

Retrieve Authenticated User’s Orders Sample Workflow
 - A GET request containing a JWT in its header is sent to the /users/myOrders endpoint.
 - API validates user is NOT an admin via JWT, returns false if validation fails.
 - If validation is successful, API retrieves orders belonging to authenticated user and returns them in its response.
// 
- A GET request containing a JWT in its header is sent to the /users/myOrders endpoint.
 - API validates user is NOT an admin via JWT, returns false if validation fails.
 - If validation is successful, API retrieves orders belonging to authenticated user and returns them in its response.

 */